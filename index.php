<?php

define('HELLO', 'hello');
define('SITE_URL', "http://localhost/huhahuha");

// 	ini_set('display_errors', 1);
// 	ini_set('display_startup_errors', 1);
// 	error_reporting(E_ALL);

// ROUTING MAP
$section = (isset($_GET['section']) && !empty($_GET['section']))?$_GET['section']:'video';

// ROUTING
switch ($section) {
	case 'video':
		require('home.php');
		break;

	case 'text':
		require('text.php');
		break;

	default:
		http_response_code(404);
		exit;
}

exit;