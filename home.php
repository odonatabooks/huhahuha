<?php
if (!defined('HELLO')) die('Oops');
?>
<!DOCTYPE html>
<html dir="ltr">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://www.youtube.com/iframe_api"></script>
	<link rel="icon" href="<?=SITE_URL?>/img/favicon.png">
	<link rel="stylesheet" href="<?=SITE_URL?>/style.css">
	<title>《呼哈！呼哈！》伴读指南</title>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-3887035-2"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-3887035-2');
	</script>
</head>

<body>
	<header>
		<img src="img/huha_main.jpg" class="responsive">
		<nav class="main">
			<ul>
				<li class="active">
					<a href="video">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="-2 -6 24 24" width="24" height="24" preserveAspectRatio="xMinYMin" class="icon"><path d="M4 2a2 2 0 0 0-2 2v4a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4a2 2 0 0 0-2-2H4zm9.98 1.605L16 1.585A2 2 0 0 1 17.414 1H18a2 2 0 0 1 2 2v6a2 2 0 0 1-2 2h-.586A2 2 0 0 1 16 10.414l-2.02-2.019A4 4 0 0 1 10 12H4a4 4 0 0 1-4-4V4a4 4 0 0 1 4-4h6a4 4 0 0 1 3.98 3.605zM17.415 9H18V3h-.586l-3 3 3 3zM5 8a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"></path></svg>
						视频
					</a>
					<hr>
				</li>
				<li class="">
					<a href="text">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="-4 -2 24 24" width="24" height="24" preserveAspectRatio="xMinYMin" class="icon"><path d="M3 0h10a3 3 0 0 1 3 3v14a3 3 0 0 1-3 3H3a3 3 0 0 1-3-3V3a3 3 0 0 1 3-3zm0 2a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H3zm2 13h2a1 1 0 0 1 0 2H5a1 1 0 0 1 0-2zm6-12a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm0 3a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm-6 6h6a1 1 0 0 1 0 2H5a1 1 0 0 1 0-2zm0-3h6a1 1 0 0 1 0 2H5a1 1 0 0 1 0-2zm.5-6h2A1.5 1.5 0 0 1 9 4.5v2A1.5 1.5 0 0 1 7.5 8h-2A1.5 1.5 0 0 1 4 6.5v-2A1.5 1.5 0 0 1 5.5 3z"></path></svg>
						文字
					</a>
					<hr>
				</li>
			</ul>
		</nav>
		<h1><span class="sticky">《呼哈！呼哈！》</span><span class="sticky">伴读指南</span></h1>
	</header>

	<div class="main-container">
		<div class="container">

			<div class="wrapper">
				
				<!-- main video -->
				<div id="main-video">
					<div id="video-frame">
						<iframe allow="fullscreen; autoplay; encrypted-media; picture-in-picture" id="video-player" src="https://www.youtube.com/embed/videoseries?list=PLOJliHo36pRQ1LszJWxN5h0CQEcxCA7VN&enablejsapi=1" width="560" height="315" frameborder="0"></iframe>
					</div>
				</div>

				<!-- video playlist -->
				<div id="playlist">

					<article class="playlist_active" data-video-index="0" data-video-id="SlkpF53A478">
						<img src="img/video-thumb/01.jpg" alt="01 本土色彩与创作目的" class="thumb">
						<div class="details">
							01 本土色彩与创作目的
						</div>
					</article>

					<article data-video-index="1" data-video-id="_tFFddAZS58">
						<img src="img/video-thumb/02.jpg" alt="02 绘本特点与创作概念" class="thumb">
						<div class="details">
							02 绘本特点与创作概念
						</div>
					</article>

					<article data-video-index="2" data-video-id="3xuEDHoyHu4">
						<img src="img/video-thumb/03.jpg" alt="03 教学建议：页2-3" class="thumb">
						<div class="details">
							03 教学建议：页2-3
						</div>
					</article>

					<article data-video-index="3" data-video-id="22fqlEbeTY8">
						<img src="img/video-thumb/04.jpg" alt="04 教学建议：页4" class="thumb">
						<div class="details">
							04 教学建议：页4
						</div>
					</article>

					<article data-video-index="4" data-video-id="ztbIE8yX9_Y">
						<img src="img/video-thumb/05.jpg" alt="05 模拟对话：页5-8" class="thumb">
						<div class="details">
							05 模拟对话：页5-8
						</div>
					</article>

					<article data-video-index="5" data-video-id="sAUhrSLLDvk">
						<img src="img/video-thumb/06.jpg" alt="06 教学建议：页9" class="thumb">
						<div class="details">
							06 教学建议：页9
						</div>
					</article>

					<article data-video-index="6" data-video-id="lzl_wfLOtnA">
						<img src="img/video-thumb/07.jpg" alt="07 教学建议：页10-11" class="thumb">
						<div class="details">
							07 教学建议：页10-11
						</div>
					</article>

					<article data-video-index="7" data-video-id="uBgWVnbwc9k">
						<img src="img/video-thumb/08.jpg" alt="08 教学建议：页12-13" class="thumb">
						<div class="details">
							08 教学建议：页12-13
						</div>
					</article>

					<article data-video-index="8" data-video-id="PfMfWgEzhYM">
						<img src="img/video-thumb/09.jpg" alt="09 教学建议：页14-15" class="thumb">
						<div class="details">
							09 教学建议：页14-15
						</div>
					</article>

					<article data-video-index="9" data-video-id="FRw68Q0OWpo">
						<img src="img/video-thumb/10.jpg" alt="10 教学建议：页28" class="thumb">
						<div class="details">
							10 教学建议：页28
						</div>
					</article>

					<article data-video-index="10" data-video-id="vlVFuabcatM">
						<img src="img/video-thumb/11.jpg" alt="11 书中的语言教学" class="thumb">
						<div class="details">
							11 书中的语言教学
						</div>
					</article>

				</div>

			</div>
			<!--wrapper-->

		</div>
		<!--container-->

		<footer>
			<img class="responsive" src="img/huha_bot.png" alt="">
		</footer>
		<!--bot image-->

	</div>
	<!--main-container-->

	<script>
		var ytplayer;
		var activeVideoIndex = 0;
		function onYouTubeIframeAPIReady() {
			ytplayer = new YT.Player('video-player', {
				events: {
					'onStateChange': onPlayerStateChange
				}
			});
		}
		function setActive (article) {
			$("article").removeClass('playlist_active');
			article.addClass('playlist_active');
			activeVideoIndex = article.data('video-index');
			
			let parent = article.parent();
			let position = article.offset().top - parent.offset().top + parent.scrollTop();
			parent.scrollTop(position);
		}
		function onPlayerStateChange (event) {
			let videoIndex = ytplayer.getPlaylistIndex();
			if (videoIndex != activeVideoIndex) {
				setActive($("article[data-video-index="+videoIndex+"]"));
			}
		}
		$("article").click(function () {
			ytplayer.playVideoAt($(this).data('video-index'));
			setActive($(this));
		});
	</script>
</body>
</html>