<?php
if (!defined('HELLO')) die('Oops');
?>
<!DOCTYPE html>
<html dir="ltr">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/lazyload@2.0.0-rc.2/lazyload.js"></script>
	<link rel="icon" href="<?=SITE_URL?>/img/favicon.png">
	<link rel="stylesheet" href="<?=SITE_URL?>/style.css">
	<title>《呼哈！呼哈！》伴读指南</title>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-3887035-2"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-3887035-2');
	</script>
</head>

<body>
	<header>
		<img src="img/huha_main.jpg" class="responsive" >
		<nav class="main">
			<ul>
				<li>
					<a href="video">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="-2 -6 24 24" width="24" height="24" preserveAspectRatio="xMinYMin" class="icon"><path d="M4 2a2 2 0 0 0-2 2v4a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4a2 2 0 0 0-2-2H4zm9.98 1.605L16 1.585A2 2 0 0 1 17.414 1H18a2 2 0 0 1 2 2v6a2 2 0 0 1-2 2h-.586A2 2 0 0 1 16 10.414l-2.02-2.019A4 4 0 0 1 10 12H4a4 4 0 0 1-4-4V4a4 4 0 0 1 4-4h6a4 4 0 0 1 3.98 3.605zM17.415 9H18V3h-.586l-3 3 3 3zM5 8a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"></path></svg>
						视频
					</a>
					<hr>
				</li>
				<li class="active">
					<a href="text">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="-4 -2 24 24" width="24" height="24" preserveAspectRatio="xMinYMin" class="icon"><path d="M3 0h10a3 3 0 0 1 3 3v14a3 3 0 0 1-3 3H3a3 3 0 0 1-3-3V3a3 3 0 0 1 3-3zm0 2a1 1 0 0 0-1 1v14a1 1 0 0 0 1 1h10a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H3zm2 13h2a1 1 0 0 1 0 2H5a1 1 0 0 1 0-2zm6-12a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm0 3a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm-6 6h6a1 1 0 0 1 0 2H5a1 1 0 0 1 0-2zm0-3h6a1 1 0 0 1 0 2H5a1 1 0 0 1 0-2zm.5-6h2A1.5 1.5 0 0 1 9 4.5v2A1.5 1.5 0 0 1 7.5 8h-2A1.5 1.5 0 0 1 4 6.5v-2A1.5 1.5 0 0 1 5.5 3z"></path></svg>
						文字
					</a>
					<hr>
				</li>
			</ul>
		</nav>
		<h1><span class="sticky">《呼哈！呼哈！》</span><span class="sticky">伴读指南</span></h1>
	</header>

	<div class="main-container">
		<div class="container">
			<div class="page-container">
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/1.jpg" src="img/thumb/1.jpg"></div>
				</div>
				<div class="btn-download">
					<a class="btn" href="guide.pdf" target="_blank">下载 PDF 伴读指南</a>
				</div>
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/2.jpg" src="img/thumb/2.jpg"></div>
					<div class="page"><img class="lazyload" data-src="img/guide/3.jpg" src="img/thumb/3.jpg"></div>
				</div>
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/4.jpg" src="img/thumb/4.jpg"></div>
					<div class="page"><img class="lazyload" data-src="img/guide/5.jpg" src="img/thumb/5.jpg"></div>
				</div>
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/6.jpg" src="img/thumb/6.jpg"></div>
					<div class="page"><img class="lazyload" data-src="img/guide/7.jpg" src="img/thumb/7.jpg"></div>
				</div>
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/8.jpg" src="img/thumb/8.jpg"></div>
					<div class="page"><img class="lazyload" data-src="img/guide/9.jpg" src="img/thumb/9.jpg"></div>
				</div>
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/10.jpg" src="img/thumb/10.jpg"></div>
					<div class="page"><img class="lazyload" data-src="img/guide/11.jpg" src="img/thumb/11.jpg"></div>
				</div>
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/12.jpg" src="img/thumb/12.jpg"></div>
					<div class="page"><img class="lazyload" data-src="img/guide/13.jpg" src="img/thumb/13.jpg"></div>
				</div>
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/14.jpg" src="img/thumb/14.jpg"></div>
					<div class="page"><img class="lazyload" data-src="img/guide/15.jpg" src="img/thumb/15.jpg"></div>
				</div>
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/16.jpg" src="img/thumb/16.jpg"></div>
					<div class="page"><img class="lazyload" data-src="img/guide/17.jpg" src="img/thumb/17.jpg"></div>
				</div>
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/18.jpg" src="img/thumb/18.jpg"></div>
					<div class="page"><img class="lazyload" data-src="img/guide/19.jpg" src="img/thumb/19.jpg"></div>
				</div>
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/20.jpg" src="img/thumb/20.jpg"></div>
					<div class="page"><img class="lazyload" data-src="img/guide/21.jpg" src="img/thumb/21.jpg"></div>
				</div>
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/22.jpg" src="img/thumb/22.jpg"></div>
					<div class="page"><img class="lazyload" data-src="img/guide/23.jpg" src="img/thumb/23.jpg"></div>
				</div>
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/24.jpg" src="img/thumb/24.jpg"></div>
					<div class="page"><img class="lazyload" data-src="img/guide/25.jpg" src="img/thumb/25.jpg"></div>
				</div>
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/26.jpg" src="img/thumb/26.jpg"></div>
					<div class="page"><img class="lazyload" data-src="img/guide/27.jpg" src="img/thumb/27.jpg"></div>
				</div>
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/28.jpg" src="img/thumb/28.jpg"></div>
					<div class="page"><img class="lazyload" data-src="img/guide/29.jpg" src="img/thumb/29.jpg"></div>
				</div>
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/30.jpg" src="img/thumb/30.jpg"></div>
					<div class="page"><img class="lazyload" data-src="img/guide/31.jpg" src="img/thumb/31.jpg"></div>
				</div>
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/32.jpg" src="img/thumb/32.jpg"></div>
					<div class="page"><img class="lazyload" data-src="img/guide/33.jpg" src="img/thumb/33.jpg"></div>
				</div>
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/34.jpg" src="img/thumb/34.jpg"></div>
					<div class="page"><img class="lazyload" data-src="img/guide/35.jpg" src="img/thumb/35.jpg"></div>
				</div>
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/36.jpg" src="img/thumb/36.jpg"></div>
					<div class="page"><img class="lazyload" data-src="img/guide/37.jpg" src="img/thumb/37.jpg"></div>
				</div>
				<div class="spread">
					<div class="page"><img class="lazyload" data-src="img/guide/38.jpg" src="img/thumb/38.jpg"></div>
					<div class="page"><img class="lazyload" data-src="img/guide/39.jpg" src="img/thumb/39.jpg"></div>
				</div>
			</div>

			<script>
			lazyload();
			</script>
		</div> <!--container-->

		<footer>
			<img class="responsive" src="img/huha_bot.png" alt="" >
		</footer> <!--bot image-->
	</div>
</body>
</html>



